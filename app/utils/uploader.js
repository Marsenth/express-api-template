const azureStorage = require('azure-storage');
const _ = require('lodash');

const upload = async ({ filePath = '', fileName = 'example', mimetype, path }) => {
  return new Promise((resolve, reject) => {
    const { env: { AZURE_STORAGE_STRING, AZURE_BLOB_SHARED } } = process;
    const blobService = azureStorage.createBlobService(AZURE_STORAGE_STRING);
    const destPath = `${_.compact(filePath.split('/')).join('/')}/${fileName}.${_.last(mimetype.split('/'))}`;
    const settings = {
      contentSettings: {
        contentType: mimetype
      }
    }

    blobService.createBlockBlobFromLocalFile(AZURE_BLOB_SHARED, destPath, path, settings, (error, result, response) => {
      if (error) {
        // file uploaded
        console.log('err => ', error)
        reject({ ...error, statusCode: 404 });
      } else {
        resolve({ ...response, body: result });
      }
    });
  });
}

module.exports = upload;