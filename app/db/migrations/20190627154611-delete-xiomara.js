'use strict'
const User = require('../../models/User');

module.exports.up = async() => {
  return await User.deleteOne({name: 'Xiomara Patiño'});
}

module.exports.down = async() => {
  return await User.create({name: 'Xiomara Patiño'});
}
