'use strict'
const User = require('../../models/User');

module.exports.up = async() => {
  await User.deleteMany({name: 'Mario Cárdenas'})
  await User.deleteMany({name: 'Diana González'});  
  await User.deleteMany({name: 'Ricardo Colmenarez'});
  await User.deleteMany({name: 'Carlos Petie'});
}

module.exports.down = async() => {
  return await User.insertMany([
    {name: 'Mario Cárdenas'},
    {name: 'Diana González'},
    {name: 'Ricardo Colmenarez'},
    {name: 'Carlos Petie'},
  ]);
}
