'use strict'
const User = require('../../models/User');

module.exports.up = async() => {
  return await User.insertMany([
    {name: 'Mario Cárdenas'},
    {name: 'Diana González'},
    {name: 'Xiomara Patiño'},
    {name: 'Ricardo Colmenarez'},
    {name: 'Carlos Petie'},
  ]);
}

module.exports.down = async() => {
  return await User.deleteMany();
}
