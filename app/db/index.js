const mongoose = require('mongoose');

const connect = () => {
  try {
    const {env: {MONGO_DB_URL}} = process;

    console.log('Connecting mongoDB.');
    mongoose.connect(MONGO_DB_URL, {
      useNewUrlParser: true,
      useFindAndModify: false,
      useCreateIndex: true,
    });
    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', function() {
      console.log('Connected to mongoDB.');
    });

    return db;
  } catch (err) {
    console.log('err =>', err);
  }
}

const disconnect = () => {
  mongoose.disconnect(function(err) {
    if (err) throw console.log(err);
  });
};

module.exports = {
  connect,
  disconnect,
};