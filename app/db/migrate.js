var DBMigrate = require('db-migrate');
const db = require('.');
const dotenv = require('dotenv');
const fs = require('fs');
const { exec } = require('child_process');
const migrationsDir = './app/db/migrations';
const _ = require('lodash');

const template = `'use strict'

module.exports.up = async() => {
  return null;
}

module.exports.down = async() => {
  return null;
}
`;

//getting an instance of dbmigrate

class Migrate {
  constructor(args) {
    const fullMethod = args[0];
    const methodArgs = (fullMethod || '').split(':');

    this.method = {
      name: _.first(methodArgs),
      param: _.size(methodArgs) > 1 ? _.last(methodArgs) : '',
    };
    
    if (!fs.existsSync(migrationsDir)) {
      fs.mkdirSync(migrationsDir);
    }
      
    this.dbmigrate = DBMigrate.getInstance(
      true,
      {
        cmdOptions: {
          'migrations-dir': migrationsDir
        },
      }
    );

    this.command = `node node_modules/db-migrate/bin/db-migrate --m='${migrationsDir}' ${args.join(' ')}`;

    switch(this.method.name) {
      case 'up':
      case 'down':
        _.find(args, (arg, i) => {
          if (arg === fullMethod) {
            const flag = (args[i+1] || '').toLowerCase();
            if (flag === '-c' || flag === '--count') {
              const count = args[i+2];
              this.count = count && Number(count);  
            } else if (flag) {
              this.name = flag;
            }
            return true;
          } else {
            return false;
          }
        });

        this[this.method.name]();
        break;
      case 'reset':
        this[this.method.name]();
        break;
      default:
        this.run();
        break;
    }
  }

  async run() {
    exec(this.command, (err, stdout, stderr) => {
      if (err) {
        console.error(stderr)
        process.exit(1);
      }

      if (this.method.name === 'create') {
        let info = stdout.split(' ');
        const path = _.last(info).replace('\n', '');
        
        fs.writeFile(path, template, 'utf-8', (err2) => {
          if (err2) {
            console.log(err2);
            process.exit(1);
          };

          info = info.slice(4, info.length).join(' ');
          console.log(info);
          process.exit(0);
        });
      } else {
        process.exit(0);
      }
    });
  }

  async up() {
    await this.dbmigrate.up(this.name || this.count, this.method.param);
    process.exit(0);
  }

  async down() {
    await this.dbmigrate.down(this.name || this.count || 1, this.method.param);
    process.exit(0);
  }

  async reset() {
    await this.dbmigrate.reset(this.method.param);
    process.exit(0);
  }
}

const init = async() => {
  try {
    dotenv.config();
    await db.connect();
    new Migrate(process.argv.slice(2, process.argv.length));
  } catch (err) {
    console.log(err);
    process.exit(1);
  }
}

init();
