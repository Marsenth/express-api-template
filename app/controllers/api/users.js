const User = require('../../models/User');
const _ = require('lodash');

exports.get = async(req, res, next) => {
  try {
    const {params: {_id}} = req;
    const user = await User.findOne({_id});
    
    if (user) {
      res.status(200).send(user);
    } else {
      res.status(404).send('Not found');
    }
  } catch (err) {
    const {message, name} = err;
    const status = name === 'MongoError' ? 400 : 500;
    res.status(status).send(message);
  }
}

exports.post = async(req, res, next) => {
  try {
    const {body} = req;
    const user = await User.create(body.user);

    if (user) {
      res.status(200).send(user);
    } else {
      res.status(404).send('Not found');
    }
  } catch (err) {
    const {message, name} = err;
    const status = name === 'MongoError' ? 400 : 500;
    res.status(status).send(message);
  }
}
