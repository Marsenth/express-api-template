const upload = require('../../utils/uploader');
const fs = require('fs');
const _ = require('lodash');

const tmpClear = (filePath) => {
  fs.unlink(filePath, (err) => {
    if (err) console.log(`Error al eliminar ${filePath}.`);
  })
}
// params: { file: <file attchment>, filePath: <string: file path>, fileName: <string: file name> }
exports.create = (req, res, next) => {
  const { file, body: { filePath, fileName } } = req;

  upload({
    ...file,
    filePath,
    fileName,
  }).then(response => {
    tmpClear(file.path);
    res.status(response.statusCode).send(response.body);
  }).catch(err => {
    tmpClear(file.path);
    res.status(err.statusCode).send(err);
  });
}
  