const express = require('express');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const createRoutes = require('./routes');
const app = express();
const dotenv = require('dotenv');
const db = require('./db');

dotenv.config();
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
createRoutes(app);
db.connect();

module.exports = app;
