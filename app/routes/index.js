const express = require('express');
const indexController = require('../controllers');
const usersRouter = require('./users');
const uploadsRouter = require('./uploads');

const router = express.Router();
const apiRouter = express.Router();

// Index route
router.get('/', indexController.index_get);

// Create routes map
const createRoutes = (app) => {
  app.use('/', router);
  router.use('/api', apiRouter);
  apiRouter.use(usersRouter);
  apiRouter.use(uploadsRouter);
}

module.exports = createRoutes;
