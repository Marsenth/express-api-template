const express = require('express');
const usersController = require('../controllers/api/users');
const router = express.Router();

router.get('/users/:_id', usersController.get);
router.post('/users', usersController.post);

module.exports = router;
