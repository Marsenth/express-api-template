const express = require('express');
const multer = require('multer');
const uploadsController = require('../controllers/api/uploads');
const router = express.Router();
const upload = multer({ dest: 'tmp/uploads/' });

router.post('/uploads', upload.single('file'), uploadsController.create);

module.exports = router;
